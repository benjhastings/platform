from botocore.exceptions import ClientError, EndpointConnectionError
from rest_framework_mongoengine.fields import FileField, DictField
from rest_framework_mongoengine.serializers import DocumentSerializer
from django.utils.translation import ugettext_lazy as _

from data.exceptions import PlatformValidationError
from data.fields import MultiPartFormDictField
from data.handlers import DataFileHandler
from data.models import Data

import logging

logger = logging.getLogger(__name__)


class DataSerializer(DocumentSerializer):
    meta = MultiPartFormDictField(write_only=True, required=False,
                     help_text=_("A dict of metadata on this "
                                 "object that you wish to store."))

    class Meta:
        model = Data
        fields = [
            'guid',
            'data',
            'meta',
            'created',
        ]
        read_only_fields = [
            'guid',
            'created',
        ]

    def __init__(self, *args, **kwargs):
        super(DataSerializer, self).__init__(*args, **kwargs)
        self.fields['data'].required = True
        self.fields['data'].allow_empty = False
        self.fields['data'].help_text = _('A dictionary of data that '
                                          'you want to save.')

    def to_representation(self, instance):
        ret = super(DataSerializer, self).to_representation(instance)

        # Only return the data from our metadata instance
        ret['meta'] = instance.metadata.data

        if instance.is_file:
            # It's a file, ensure we return specific internal data.
            self.handler = DataFileHandler(user=self.context['request'].user,
                                           file=instance)
            #TODO: Decide want we want our metadata to be returned as
            ret['data'] = self.handler.to_representation_data()
            ret['meta'] = self.handler.to_representation_metadata()
        return ret

    def validate(self, attrs):
        """
        Build the metadata for storage in the system.  The user will post
        "meta" to us but we need to store it in the db as "metadata" (because
        of conflicting attribute names).  The data that the user posts us
        will be stored in data.models.Metadata.data.
        """
        attrs['metadata'] = {
            'data': attrs.pop('meta', None),
            'user_id': self.context['request'].user.pk
        }
        return attrs


class DataFileSerializer(DataSerializer):
    file = FileField(required=True,
                     write_only=True,
                     help_text=_("The file that you wish to upload"))

    class Meta(DataSerializer.Meta):
        model = Data
        fields =[
            'guid',
            'data',
            'file',
            'meta',
            'created',
        ]
        read_only_fields =[
            'guid',
            'data',
            'created',
        ]

    def validate(self, attrs):
        attrs = super(DataFileSerializer, self).validate(attrs)
        file = attrs['file']
        self.handler = DataFileHandler(user=self.context['request'].user,
                                       file=file)
        # Add any internal metadata on
        attrs['metadata'].update(self.handler._internal_metadata())
        attrs.pop('file')
        return attrs

    def create(self, validated_data):
        """
        Uploads the file to S3 and saves the related data to the DB.  As this
        is backed by Mongo which doesn't have transactions we attempt to upload
        the file first as this is most likely to fail
        :param validated_data: The data that we want to savae
        :return: Either ValidationError or the data that was saved
        """
        try:
            self.handler.upload()
        except (EndpointConnectionError, ClientError) as e:
            # EndpointConnectionError - No network connection to S3
            # ClientError - Generic ClientError. Could be many things
            logger.exception(e)
            raise PlatformValidationError({'file': _('An error occurred '
                                                     'saving this file.'
                                                     ' Please try again')})
        data = super(DataFileSerializer, self).create(validated_data)

        return data

from rest_framework import mixins, status
from rest_framework.parsers import FileUploadParser, MultiPartParser, \
    JSONParser, FormParser
from rest_framework.response import Response
from rest_framework_mongoengine.viewsets import GenericViewSet

from data.exceptions import PlatformValidationError
from data.models import Data
from data.parsers import MultiPartFileAndJSONParser
from data.serializers import DataSerializer, DataFileSerializer


class BaseItemViewSet(GenericViewSet):
    lookup_field = 'guid'
    queryset = Data.objects.none()
    serializer_class = DataSerializer

    def get_queryset(self):
        return Data.for_user(user=self.request.user)



#TODO: Decide what methods we want to expose
#TODO: Paginate list response?
class DataViewSet(mixins.CreateModelMixin,
                  mixins.RetrieveModelMixin,
                  mixins.ListModelMixin,
                  BaseItemViewSet):
    """
    Endpoint for creating items.  This accepts either files or a
    JSON dictionary.  See verbs for examples.
    retrieve:
        Retrieves a single data/file item that you uploaded.
        Example: requests.get('<url>', headers={'Authorization': 'Token <your-token>'})
    list:
        Retrieves all data/file items that you uploaded.
        Example: requests.get('<url>', headers={'Authorization': 'Token <your-token>'})
    create:
        Creates a single data item.
        Example for data: requests.post('<url>', json=<dict>, headers={'Authorization': 'Token <your-token>'})
    """
    pass


class ItemFileViewSet(mixins.CreateModelMixin,
                      BaseItemViewSet):
    """
    Endpoint for creating file items. See verbs for examples.
    Use the /data endpoint for retrieving all data.

    create:
        Creates a single file data item.
        Example for file: requests.post('<url>',
                                        files=<file>,
                                        data={'meta': json.dumps(<data>)},
                                        headers={'Authorization': 'Token <your-token>'})
    """
    parser_classes = (MultiPartFileAndJSONParser,)
    serializer_class = DataFileSerializer

    def create(self, request, *args, **kwargs):
        try:
            resp = super(ItemFileViewSet, self).create(request, *args, **kwargs)
        except PlatformValidationError as e:
            # There was a problem uploading the file
            return Response(e.error_dict, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        return resp

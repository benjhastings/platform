import json

from botocore.exceptions import ClientError
from django.conf import settings
from django.contrib.auth import get_user_model
from mock import patch
from model_mommy import mommy
from model_mommy.random_gen import gen_file_field, gen_image_field
from rest_framework import status
from rest_framework.authtoken.models import Token
from rest_framework.reverse import reverse
from rest_framework.test import APIRequestFactory, force_authenticate, \
    APIClient

from data.models import Data
from data.viewsets import DataViewSet, ItemFileViewSet
from platform_django.testcases import MongoTestCase


class TestDataViewSet(MongoTestCase):
    def setUp(self):
        self.user = mommy.make(get_user_model())
        self.factory = APIRequestFactory()
        self.url = reverse('api:item-list')
        self.view = DataViewSet.as_view({'post': 'create'})

    def test_create_data_data_argument_required(self):
        """
        Tests the creation of data in mongoDB
        """
        data = {}
        request = self.factory.post(self.url,
                                    json.dumps(data),
                                    content_type='application/json')
        force_authenticate(request, user=self.user)
        resp = self.view(request)
        # Test that the data argument is required
        assert resp.status_code == status.HTTP_400_BAD_REQUEST
        assert str(resp.data['data'][0]) == 'This field is required.'

    def test_create_data_data_argument_cant_be_empty(self):
        """
        Tests that the data argument can't be empty
        :return:
        """
        data = {
            'data': {}
        }
        request = self.factory.post(self.url,
                                    json.dumps(data),
                                    content_type='application/json')
        force_authenticate(request, user=self.user)

        resp = self.view(request)
        # Test that the data argument is not empty
        assert resp.status_code == status.HTTP_400_BAD_REQUEST
        error_msg = str(resp.data['data']['non_field_errors'][0])
        assert error_msg == 'This dict may not be empty.'

    def test_create_data_success(self):
        """
        Tests successful data creation
        :return:
        """
        data = {
            'data': {
                "key1": "string",
                "key2": 153525483.1285,
                "a_list": [
                    "string",
                    1, 2.3648,
                    ["nested", "lists", "are", "cool"],
                    {"and": "dictionaries too"}
                ],
            },
            'meta': {
                'some': 'data',
                'list': ['list', 'here']
            }
        }
        request = self.factory.post(self.url,
                                    json.dumps(data),
                                    content_type='application/json')
        force_authenticate(request, user=self.user)

        assert Data.objects.count() == 0
        resp = self.view(request)
        # Test that the data argument is not empty
        assert resp.status_code == status.HTTP_201_CREATED
        assert resp.data['meta'] == {
            'some': 'data',
            'list': ['list', 'here']
        }
        data = Data.objects.all()

        assert data[0].metadata.data == {
            'some': 'data',
            'list': ['list', 'here']
        }
        assert data[0].metadata.user_id == self.user.pk
        assert data.count() == 1

    def test_retrieve(self):
        """
        Tests the retrieval method
        :return:
        """
        # Make two sets of data to test the queryset
        id = 'some-unique-id'
        data = {
            'data': {
                'id': id,
                'some-data': 'here',
                'nested': {'nested': 'thing'}
            },
            'meta': {'some': 'data'}
        }
        request = self.factory.post(self.url,
                                    json.dumps(data),
                                    content_type='application/json')
        force_authenticate(request, user=self.user)
        self.view(request)
        user2 = mommy.make(get_user_model())
        # This will not be retrieved because it was uploaded by a different
        # user
        data = {
            'data': {
                'id': 'some-different-unique-id',
                'some-data': 'here',
                'nested': {'nested': 'thing'}
            }
        }
        request = self.factory.post(self.url,
                                    json.dumps(data),
                                    content_type='application/json')
        force_authenticate(request, user=user2)
        self.view(request)
        assert Data.objects.count() == 2

        # Do the retrieve
        view = DataViewSet.as_view({'get': 'list'})
        request = self.factory.get(self.url)
        force_authenticate(request, self.user)
        resp = view(request)
        assert len(resp.data) == 1
        assert resp.data[0]['data']['id'] == id


class TestItemFileViewSet(MongoTestCase):
    def setUp(self):
        self.user = mommy.make(get_user_model(), is_active=True)
        self.token = mommy.make(Token, user=self.user)

        self.url = reverse('api:item-file-list')

        self.files = {
            'file': gen_file_field()
        }

        self.client.credentials(HTTP_AUTHORIZATION='Token ' + self.token.key)

    @patch('boto3.client')
    def test_upload_failure(self, mocked_client):
        """
        Tests the creation of files in MongoDB and on S3
        :return:
        """
        # Test if there is an error uploading the file
        assert Data.objects.count() == 0
        mocked_client.return_value.put_object.side_effect = ClientError({}, 'err')  # noqa
        resp = self.client.post(self.url, self.files, format='multipart')

        assert resp.status_code == status.HTTP_500_INTERNAL_SERVER_ERROR

        assert resp.data['file'][0].messages[0] == 'An error occurred saving '\
                                                   'this file. Please try ' \
                                                   'again'

    @patch('boto3.client')
    @patch('data.handlers.uuid')
    def test_successful_upload(self, mocked_uuid, mocked_client):
        """
        Tests that a file was successfully uploaded
        """
        mocked_client.return_value.generate_presigned_url.return_value = 'http://some-presigned-url.com'
        mocked_uuid.uuid4.return_value = 'b052eac9-f37d-4a3f-afcd-3c9eea522178'
        resp = self.client.post(self.url, self.files, format='multipart')

        assert resp.status_code == status.HTTP_201_CREATED

        data = Data.objects.all()
        assert data.count() == 1
        data = data[0]
        assert data.is_file is True
        assert data.metadata.is_file is True
        assert data.metadata.internal['is_file'] is True, 'Ensure that ' \
                                                          'internal metadata '\
                                                          'has "is_file" key '\
                                                          'for files'
        assert mocked_client.called is True
        # Check that the file was uploaded correctly.  Can't compare the body
        internal_filename = '{0}-{1}'.format(mocked_uuid.uuid4.return_value,
                                             self.files['file'].name)
        key = '{user_id}/{internal_filename}'.format(
            user_id=self.user.pk,
            internal_filename=internal_filename
        )
        ContentDisposition = 'attachment; filename={0}'.format(self.files['file'].name)  # noqa
        assert mocked_client.return_value.put_object.call_count == 1
        assert mocked_client.return_value.put_object.call_args[1]['Bucket'] \
               == settings.DATA_BUCKET_NAME
        assert mocked_client.return_value.put_object.call_args[1][
                   'ContentDisposition'] == ContentDisposition
        assert mocked_client.return_value.put_object.call_args[1][
                   'Key'] == key

    @patch('boto3.client')
    @patch('data.handlers.uuid')
    def test_internal_data_keys_exist(self, mocked_uuid, mocked_client):
        """
        When uploading files we require certain data keys to be created.
        This test ensures that they are there.
        :return:
        """
        mocked_client.return_value.generate_presigned_url.return_value = 'http://some-presigned-url.com'
        mocked_uuid.uuid4.return_value = 'b052eac9-f37d-4a3f-afcd-3c9eea522178'
        resp = self.client.post(self.url, self.files, format='multipart')
        assert resp.status_code == status.HTTP_201_CREATED
        data = Data.objects.all()[0]
        expected_internal_data = {'is_file': True,
                                  'internal_filename': 'b052eac9-f37d-4a3f-afcd-3c9eea522178-mock_file.txt',  # noqa
                                  'size': 10,
                                  'filename': 'mock_file.txt'}

        assert data.metadata.internal == expected_internal_data, \
            'Internal data missing.  If you have changed how these are stored ' \
            'please ensure you do a data migration for existing data'

    @patch('boto3.client')
    @patch('data.handlers.uuid')
    def test_file_and_data_endpoints_return_same_data(self, mocked_uuid,
                                                      mocked_client):
        mocked_client.return_value.generate_presigned_url.return_value = 'http://some-presigned-url.com'
        mocked_uuid.uuid4.return_value = 'b052eac9-f37d-4a3f-afcd-3c9eea522178'
        meta = {
            'some': {'nested': ['meta', 'data']}
        }
        # file metadata must be JSON

        data = {
            'meta': json.dumps(meta),
            'file': gen_file_field()
        }
        resp = self.client.post(self.url,
                                data,
                                format='multipart')
        create_return_file_data = resp.data

        data = {
            'data': {
                'id': 'some-unique-id',
                'some-data': 'here',
                'nested': {'nested': 'thing'}
            },
            'meta': meta
        }

        view = DataViewSet.as_view({'post': 'create'})
        request = APIRequestFactory().post(reverse('api:item-list'),
                                           json.dumps(data),
                                           content_type='application/json')
        force_authenticate(request, user=self.user)
        resp = view(request)

        create_return_item_data = resp.data

        assert create_return_item_data.keys() == create_return_file_data.keys(), \
            'Expected file data and item data to have the same keys at ' \
            'creation'

        assert create_return_item_data['meta'] == create_return_file_data['meta'], \
            'Metadata differs between file and item created data'

        url = reverse('api:item-detail',
                      args=(create_return_item_data['guid'],))
        resp = self.client.get(url,)
        retrieve_item_data = resp.data

        url = reverse('api:item-detail',
                      args=(create_return_file_data['guid'],))
        resp = self.client.get(url, )
        retrieve_file_data = resp.data

        assert retrieve_item_data.keys() == retrieve_file_data.keys()
        assert 'created' in retrieve_item_data.keys()
        assert 'created' in retrieve_file_data.keys()
        # For some reason when retrieving the created date doesn't format
        # the microseconds correctly.
        del retrieve_item_data['created']
        del retrieve_file_data['created']
        del create_return_item_data['created']
        del create_return_file_data['created']

        # Ensure the data is the same when creating or retrieving
        assert retrieve_item_data == create_return_item_data, 'Created and ' \
                                                              'retrieved ' \
                                                              'data differs'
        assert retrieve_file_data == create_return_file_data, 'Created and ' \
                                                              'retrieved data differs'

from django.conf import settings
from django.http import QueryDict
from django.utils import six
from rest_framework.parsers import MultiPartParser, DataAndFiles
from rest_framework.settings import api_settings
from rest_framework.utils import json
from rest_framework.exceptions import ParseError
from django.http.multipartparser import \
    MultiPartParser as DjangoMultiPartParser, MultiPartParserError


class MultiPartFileAndJSONParser(MultiPartParser):
    """
    A parser which enables a user to post a file and some json data to a
    viewset.
    I.E. requests.post(<url>,
                       files=files,
                       data={'key': json.dumps({'some':'data'}))
    """
    strict = api_settings.STRICT_JSON

    def parse(self, stream, media_type=None, parser_context=None):
        parser_context = parser_context or {}
        request = parser_context['request']
        encoding = parser_context.get('encoding', settings.DEFAULT_CHARSET)
        meta = request.META.copy()
        meta['CONTENT_TYPE'] = media_type
        upload_handlers = request.upload_handlers
        try:
            parser = DjangoMultiPartParser(meta, stream, upload_handlers, encoding)
            data, files = parser.parse()
            data = self.load_json(data)
            qdt = QueryDict('', mutable=True)
            qdt.update(data)
            return DataAndFiles(qdt, files)
        except MultiPartParserError as exc:
            raise ParseError('Multipart form parse error - %s' % six.text_type(exc))

    def load_json(self, data):
        """
        Takes the data that was posted with the multipart/form-data request and
        iterates the key/value pairs and attempts to load the value as json
        into the key
        :param data: dict of key value json strings sent with the request
        :return: dict of key value json strings that have been parsed
        """
        ret = {}
        for key, value in data.items():
            try:
                parse_constant = json.strict_constant if self.strict else None
                ret[key] = json.loads(value, parse_constant=parse_constant)
            except ValueError as exc:
                raise ParseError('JSON parse error - %s' % six.text_type(exc))
        return ret

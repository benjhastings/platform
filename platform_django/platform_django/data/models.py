import uuid

from mongoengine import EmbeddedDocument, EmbeddedDocumentField, \
    queryset_manager
from mongoengine import fields

from misc.models import TimeStampedMongoModel


class MetaData(EmbeddedDocument):
    data = fields.DictField()
    # A field to use for internal metadata that we might not want to return
    internal = fields.DictField()
    user_id = fields.IntField(required=True)

    @property
    def is_file(self):
        return self.internal.get('is_file', False)

#TODO: Probably rename this to "Item" as that's what it is in our nomenclature
class Data(TimeStampedMongoModel):
    guid = fields.UUIDField()
    data = fields.DictField()
    metadata = EmbeddedDocumentField(MetaData)

    def save(self, *args, **kwargs):
        if not self.guid:
            self.guid = uuid.uuid4()
        return super(Data, self).save(*args, **kwargs)

    @queryset_manager
    def for_user(doc_cls, queryset, user):
        """
        Manager for getting the docs that belong to a specific user
        :param queryset: Queryset of Data items
        :param user: The users.User object that we want to filter by
        :return: The queryset filtered by the user.
        """
        return queryset.filter(metadata__user_id=user.pk)

    @property
    def is_file(self):
        return self.metadata.is_file

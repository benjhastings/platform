from django.core.exceptions import ValidationError


class PlatformValidationError(ValidationError):
    """
    Make it easier for us to handle validation errors in a platform specifci
    way
    """
    pass

import uuid

import boto3
from botocore.config import Config
from django.conf import settings

from data.models import Data


class DataFileHandler(object):
    """
    A handler for uploading a data file to S3 and handling things like
    presigned URL generation and metadata
    """
    def __init__(self, user, file, *args, **kwargs):
        """
        __init__ method
        :param user: An instance of users.user.  Every upload is tied to a user
        :param file:  Either a File instance or an instance of data.Data that
                      contains relevant metadata for us to look for a file
        :param args: *args
        :param kwargs: **kwargs
        """
        self._internal_filename = None
        self.user = user
        self.file = file
        region_name = kwargs.get('region_name', 'eu-west-2')
        self.client = boto3.client('s3',
                                   region_name=region_name,
                                   config=Config(signature_version='s3v4',
                                                 s3={'addressing_style': 'path'}))  # noqa7

    def _internal_metadata(self):
        """
        Builds how our internal metadata should be structured.  Be very careful
        when changing this as values here will be relied upon all over the
        place
        """
        return {
            'internal': {
                'is_file': True,
                'internal_filename': self.get_internal_filename(),
                'size': self.file.size,
                'filename': self.file.name,
            },
        }

    def to_representation_data(self):
        """
        A dictionary of the specific data we want to return.  As users don't
        post "data" with files this is something we have to construct
        :return:
        """
        return {
            'size': self.file.metadata.internal['size'],
            'url': self.generate_url(),
            'filename': self.file.metadata.internal['filename'],
        }

    def to_representation_metadata(self):
        """
        A dictionary of the specific metadata that we want to return to the
        user
        """
        return self.file.metadata.data

    def get_internal_filename(self):
        """
        Generates a randomised filename for use internally to avoid
        filenames clashing.  Only generates this once per instantiation
        of the handle
        :return: <uuid>/<self.file.name>
        """
        if self._internal_filename is None:
            self._internal_filename = '{0}-{1}'.format(
                str(uuid.uuid4()), self.file.name
            )
        return self._internal_filename

    def get_key(self, internal_filename=None):
        """
        Builds the full key to be stored in S3
        :return: <user_id>/<self.internal_filename>
        """
        if internal_filename is None:
            internal_filename = self.get_internal_filename()
        return '{user_id}/{internal_filename}'.format(
            user_id=self.user.pk,
            internal_filename=internal_filename
        )

    def upload(self):
        key = self.get_key()
        ContentDisposition = 'attachment; filename={0}'.format(self.file.name)
        self.client.put_object(Bucket=settings.DATA_BUCKET_NAME,
                               ContentDisposition=ContentDisposition,
                               Key=key,
                               Body=self.file)

    def generate_url(self):
        assert isinstance(self.file, Data), '{0}.file Must be instance of ' \
                                            '{1}'.format(self.__class__, Data)
        key = self.get_key(self.file.metadata.internal['internal_filename'])
        Params = {
            'Bucket': settings.DATA_BUCKET_NAME,
            'Key': key
        }
        url = self.client.generate_presigned_url('get_object',
                                                 Params=Params)
        return url

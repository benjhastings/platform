from rest_framework.fields import empty
from rest_framework_mongoengine.fields import DictField


class MultiPartFormDictField(DictField):
    """
    Allows us to post json data along with form data to the file endpoint.
    We don't care about dictionaries in HTML form so that is completely
    removed here.
    """
    def get_value(self, dictionary):
        return dictionary.get(self.field_name, empty)

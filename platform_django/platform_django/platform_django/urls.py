"""platform_django URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf import settings
from django.urls import path, include
from rest_framework.authtoken import views as auth_token_views
from rest_framework.documentation import include_docs_urls
from rest_framework_mongoengine import routers
from data.viewsets import DataViewSet, ItemFileViewSet
from platform_django.admin import admin_site

router = routers.SimpleRouter(trailing_slash=False)
router.register(r'data', DataViewSet, base_name='item')
router.register(r'data-file', ItemFileViewSet, base_name='item-file')

urlpatterns = [
    path('api-token-auth/', auth_token_views.obtain_auth_token),
    path('api/', include((router.urls, 'api'))),
    path('docs/', include_docs_urls(title='The Platform',
                                    #TODO: Remove /dev when we have it hosted under a proper url
                                    schema_url=None if settings.DEBUG else '/dev',
                                    authentication_classes=[],
                                    permission_classes=[])),
    path('admin/', admin_site.urls),
]

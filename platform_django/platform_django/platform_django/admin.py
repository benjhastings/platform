from django.contrib.admin import AdminSite as BaseAdminSite
from django.utils.translation import ugettext_lazy


class AdminSite(BaseAdminSite):
    site_title = ugettext_lazy('The Platform')
    site_header = ugettext_lazy('The Platform administration')
    index_title = ugettext_lazy('The Platform administration')


admin_site = AdminSite()

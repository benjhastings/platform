import os

from zappa_secrets_manager.load_secrets import load_secrets


ENV_PATH = os.path.dirname(os.path.abspath(__file__))
EXTRA_ENVS_TO_SET = [
    ('DJANGO_DATABASES_PASSWORD', '{PROJECT_NAME}/{STAGE}/db/{PROJECT_NAME}'),
    ('MONGO_DATABASES_PASSWORD', '{PROJECT_NAME}/{STAGE}/mongodb/{PROJECT_NAME}')  # noqa
]


load_secrets(PROJECT_NAME='aelplatform',
             ENV_PATH=ENV_PATH,
             EXTRA_ENVS_TO_SET=EXTRA_ENVS_TO_SET)

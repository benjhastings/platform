from django.conf import settings
from django.test import TestCase
from rest_framework.test import APITestCase


class MongoTestCase(APITestCase):
    """
    TestCase class that clear the collection between the tests
    """
    def _post_teardown(self):
        from mongoengine.connection import get_connection
        connection = get_connection()
        connection.drop_database(settings.MONGODB_SETTINGS['db'])
        super(MongoTestCase, self)._post_teardown()

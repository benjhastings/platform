import os
# We set this here before doing any other import so that we can guarantee
# that we are using the test database
os.environ['MONGO_DATABASES_NAME'] = 'test_aelplatform'

from platform_django.settings import *


DATA_BUCKET_NAME = 'completelyinvalidbucketname'

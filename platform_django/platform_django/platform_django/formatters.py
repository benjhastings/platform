from json_log_formatter import JSONFormatter


class CustomJSONFormatter(JSONFormatter):
    def extra_from_record(self, record):
        """
        This is pinched from the source for
        https://github.com/vklochan/python-logstash/blob/master/logstash/formatter.py
        Hopefully this should enable us to serializer WSGIRequest to JSON
        and actually provide relevant stack traces
        """
        skip_list = (
            'args', 'asctime', 'created', 'exc_info', 'exc_text', 'filename',
            'funcName', 'id', 'levelname', 'levelno', 'lineno', 'module',
            'msecs', 'msecs', 'message', 'msg', 'name', 'pathname', 'process',
            'processName', 'relativeCreated', 'thread', 'threadName', 'extra',
            'auth_token', 'password')

        easy_types = (str, bool, dict, float, int, list, type(None))

        fields = {}

        for key, value in record.__dict__.items():
            if key not in skip_list:
                if isinstance(value, easy_types):
                    fields[key] = value
                else:
                    fields[key] = repr(value)

        return fields

    def json_record(self, message, extra, record):
        resp = super(CustomJSONFormatter, self).json_record(message,
                                                            extra,
                                                            record)
        # Add in status for easier interpretation by 3rd party log handlers
        resp['status'] = record.levelname.upper()
        return resp

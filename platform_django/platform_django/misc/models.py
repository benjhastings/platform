from datetime import datetime

from django.db import models
from mongoengine import Document, fields


class TimeStampedModel(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True


class TimeStampedMongoModel(Document):
    meta = {
        'abstract': True,
    }
    created = fields.DateTimeField()
    modified = fields.DateTimeField(default=datetime.utcnow)

    def save(self, *args, **kwargs):
        if not self.created:
            self.created = datetime.utcnow()
        self.modified = datetime.utcnow()
        return super(TimeStampedMongoModel, self).save(*args, **kwargs)

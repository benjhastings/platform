from flask import Flask
from flask_login import LoginManager

from config import DevelopmentConfig
from auth.models import User

from flask_migrate import Migrate

migrate = Migrate()


def create_app():
    app = Flask(__name__)

    app.config.from_object(DevelopmentConfig)
    # mdb.connect(host="mongodb+srv://aelweb:Agiletek123@aelweb1-x9mab.mongodb.net/aelweb")
    # from aelweb.website.controllers import aelweb
    # from aelweb.website.auth import auth
    # from aelweb.website.auth.models import User

    login_manager = LoginManager()
    login_manager.init_app(app)
    login_manager.login_view = "auth.login"

    @login_manager.user_loader
    def load_user(user_id):
        try:
            return User.objects.get(id=user_id)
        except User.DoesNotExist:
            return None
    #
    # app.register_blueprint(auth)
    #
    # app.register_blueprint(aelweb)
    from auth.models import db
    db.init_app(app)

    migrate.init_app(app, db)

    return app

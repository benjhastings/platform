

from flask_login import UserMixin
# from mongoengine import Document, EmailField, StringField
#from app import db
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.orm import validates

db = SQLAlchemy()


class User(UserMixin,db.Model):
    __tablename__ = 'auth_user'
    id = db.Column(db.Integer, primary_key=True)
    # username = db.Column(db.String(80), unique=True, nullable=False)
    email = db.Column(db.String(120), unique=True, nullable=False)
    password = db.Column(db.String(120))

    # def __repr__(self):
    #     return '<User %r>' % self.username
#
# class User(db.Model, UserMixin):
#     __tablename__ = 'user'
#     id = db.Column(db.Integer, primary_key=True)
#     email = db.Column(db.String)
#     password = db.Column(db.String)
#
#     def __init__(self, url, result_all, result_no_stop_words):
#         self.url = url
#         self.result_all = result_all
#         self.result_no_stop_words = result_no_stop_words
#
#     def __repr__(self):
#         return '<id {}>'.format(self.id)
#
#     @validates('email')
#     def validate_email(self, key, address):
#         assert '@' in address
#         return address
#
#     def save(self):
#         return super
